let words = ["sacrificio","roto","puerza","corazon","anarquia","comunismo","capitalismo","rebelión","musica","arte","artista","persona","Dios","menos","mas","gato","perro","elefante","orangutan","simio"]
let vocals = ["a","e","i","o","u"];
let vocals_combinations = ["ae","ai","ao","au","ei","eo","eu","io","iu","ou","ea","ia","oa","ua","ie","oe","ue","oi","ui","ou",]

const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function ask(questionText) {
    return new Promise((resolve, reject) => {
      rl.question(questionText, resolve);
    });
  }

async function main() {
    let selection = await ask(`elige un numero entre el 1 y el 3.\nel 1 imprime todas las palabras que comienzan con vocal.\nel 2 imprime todas las palabras que comienzan por consonante.\ny el 3 imprime todas las palabras que tienen 2 vocales consecutivas.\ntu elección es: `)

    switch (selection){

        case "1":
            for(let i = 0;i<words.length;i++){
                for (let x = 0;x<vocals.length;x++){
                    if (words[i].charAt(0)==vocals[x]){
                        console.log(words[i])
                    }
                }
            }
            break;

        case "2":
            for(let i = 0;i<words.length;i++){
                if (words[i].charAt(0)!=vocals[0] && words[i].charAt(0)!=vocals[1] && words[i].charAt(0)!=vocals[2] && words[i].charAt(0)!=vocals[3] && words[i].charAt(0)!=vocals[4]){
                    console.log(words[i])
                }
            }
            break;
            
        case "3":
            for(let i = 0;i<words.length;i++){
                for (let x = 0;x<vocals_combinations.length;x++){
                    if (words[i].match(vocals_combinations[x])){
                        console.log(words[i])
                    }
                }
            }
            break;
    }

    process.exit(1);
}

main()