let people =[
{firstname:"juan alberto",lastname:"alvarez sosa",idnumber:01,height:1.75},
{firstname:"juan camilo",lastname:"niño gonzales",idnumber:02,height:1.65},
{firstname:"andres santiago",lastname:"alvarez sosa",idnumber:03,height:1.70},
{firstname:"ivan andres",lastname:"gonzales sosa",idnumber:04,height:1.50},
{firstname:"juana isabella",lastname:"mendez mendez",idnumber:05,height:1.65},
{firstname:"maria josé",lastname:"martinez mora",idnumber:06,height:1.60},
{firstname:"sofia valentina",lastname:"zamora nieto",idnumber:07,height:1.62},
{firstname:"juana isabel",lastname:"leon mora",idnumber:08,height:1.50}
];

console.log(" ")

let peopleLastname = people.sort( (a,b)=>{
    return a.lastname.localeCompare(b.lastname);
} );

peopleLastname.forEach((i,idx)=>{
    console.log(`persona: ${idx} apellido: ${i.lastname} nombre: ${i.firstname} numero de identificación: ${i.idnumber} estatura: ${i.height}`)
});

console.log(" ")

let peopleFirstname = people.sort( (a,b)=>{
    return a.firstname.localeCompare(b.firstname);
} );

peopleFirstname.forEach((i,idx)=>{
    console.log(`persona: ${idx} nombre: ${i.firstname} apellido: ${i.lastname} numero de identificación: ${i.idnumber} estatura: ${i.height}`)
});

console.log(" ")

let peopleIdnumber = people.sort( (a,b)=>b.idnumber-a.idnumber);

peopleIdnumber.forEach((i,idx)=>{
    console.log(`persona: ${idx} numero de identificación: ${i.idnumber} apellido: ${i.lastname} nombre: ${i.firstname} estatura: ${i.height}`)
});

console.log(" ")

let peopleHeight = people.sort( (a,b)=>b.height-a.height );

peopleHeight.forEach((i,idx)=>{
    console.log(`persona: ${idx} estatura: ${i.height} apellido: ${i.lastname} nombre: ${i.firstname} numero de identificación: ${i.idnumber}`)
});

console.log(" ")

let totalHeight=people.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población: ${totalHeight/people.length}`);

console.log(" ");

let peopleFilteredA=people.filter( (i)=>{
    return i.lastname.match(/\ba\w*/i);
} );

let totalHeightA=peopleFilteredA.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "A": ${totalHeightA/peopleFilteredA.length}`);

let peopleFilteredG=people.filter( (i)=>{
    return i.lastname.match(/\bg\w*/i);
} );

let totalHeightG=peopleFilteredG.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "G": ${totalHeightG/peopleFilteredG.length}`);

let peopleFilteredL=people.filter( (i)=>{
    return i.lastname.match(/\bl\w*/i);
} );

let totalHeightL=peopleFilteredL.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "L": ${totalHeightL/peopleFilteredL.length}`);

let peopleFilteredM=people.filter( (i)=>{
    return i.lastname.match(/\bm\w*/i);
} );

let totalHeightM=peopleFilteredM.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "M": ${totalHeightM/peopleFilteredM.length}`);

let peopleFilteredN=people.filter( (i)=>{
    return i.lastname.match(/\bn\w*/i);
} );

let totalHeightN=peopleFilteredN.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "N": ${totalHeightN/peopleFilteredN.length}`);

let peopleFilteredZ=people.filter( (i)=>{
    return i.lastname.match(/\bz\w*/i);
} );

let totalHeightZ=peopleFilteredZ.reduce( (acc,cur)=>{
    return acc+cur.height;
}, 0 );

console.log(`altura promedio de la población cuyo apellido comienza con "Z": ${totalHeightZ/peopleFilteredZ.length}`);