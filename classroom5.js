const primes = require('./classroom1')

class Calcfactorial{
    constructor(){
        this.number;
        this.facnumber;
    }

    calcfactorial(number){
        this.number = number;
        this.facnumber = number - 1;

        for (let i = this.facnumber;i!=0;i--){
            this.number = this.number * this.facnumber;
            this.facnumber--;
        }
        return this.number
    }

}

const holi = new Calcfactorial();

console.log(`el factorial de: 20 es: ${holi.calcfactorial(20)}\n`);

for (let i = 0; i != primes.logprimes.length; i++){
    let factorial = holi.calcfactorial(primes.logprimes[i]);
    console.log(`el factorial de: ${primes.logprimes[i]} es: ${factorial}`);
};