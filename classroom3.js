var namesnice = [];
var namesbad = [];

class Names{

    constructor(firstname1,firstname2,lastname1,lastname2){
        this.names=[];
        this.name= "";
        this.names.push(firstname1);
        this.names.push(firstname2);
        this.names.push(lastname1);
        this.names.push(lastname2);
    }

    formnameundefined(){
        this.name = this.names[0] + " " + this.names[1] + " " + this.names[2] + " " + this.names[3]
        return this.name;
    }

    formname(){
        for(let i=0;i<this.names.length;i++){
            if(this.names[i]==undefined){
                break;
            } else {
                this.name = this.name+ this.names[i] + " " ;
            }
        }
        return this.name;
    }

};

const name1 = new Names("juana", "isabel","leon","mora");
const name2 = new Names("david","santiago","gutierrez");
const name3 = new Names("jorge","andres","perez", "mora");
const name4 = new Names("andres","santiago","gonzales", "niño");
const name5 = new Names("nestor","escamilla","grande");
const name6 = new Names("omar","chaparro");
const name7 = new Names("juan","david","guatibonza","ramirez");

namesnice.push(name1.formname());
namesbad.push(name1.formnameundefined());

namesnice.push(name2.formname());
namesbad.push(name2.formnameundefined());

namesnice.push(name3.formname());
namesbad.push(name3.formnameundefined());

namesnice.push(name4.formname());
namesbad.push(name4.formnameundefined());

namesnice.push(name5.formname());
namesbad.push(name5.formnameundefined());

namesnice.push(name6.formname());
namesbad.push(name6.formnameundefined());

namesnice.push(name7.formname());
namesbad.push(name7.formnameundefined());

console.log(namesnice);

console.log(namesbad);