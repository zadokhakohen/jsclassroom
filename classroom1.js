class Primos {
    constructor(){
        this.number = 0;
        this.primenumber = true;
        this.number_prime = 0;
        this.numbercounter = 2;
        this.hundredprimes = [];
    }

    isprime(number){

        this.number = number;

        for (let i = this.number - 1;i != 1; i--){
            
            let num = this.number/i;

            if (num - Math.floor(num) == 0) {
                this.primenumber = false;
            } else {
                this.primenumber = true;
            }
        }

        return this.primenumber;
    }

    logprimes() {

        for(let x = 1; x <= 100; x = x + this.number_prime) {

            if(this.isprime(this.numbercounter)) {

                this.hundredprimes.push(this.numbercounter);
                this.number_prime = 1;

            } else {

                this.number_prime = 0;

            }
            this.numbercounter++;
        }
    }

}

const prime = new Primos();

//console.log(`el numero es primo? ${prime.isprime(5)}`);

prime.logprimes();

//console.log(prime.hundredprimes);

logprimes = prime.hundredprimes;

module.exports={logprimes};